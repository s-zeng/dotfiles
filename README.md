# Dotfiles
Dot files for my linux setup

[Screenshot 1](https://raw.githubusercontent.com/s-zeng/vim-settings/master/Pictures/Desktop1.png)

[Screenshot 2](https://raw.githubusercontent.com/s-zeng/vim-settings/master/Pictures/Desktop3.png)

[Screenshot 3](https://raw.githubusercontent.com/s-zeng/vim-settings/master/Pictures/Desktop3clean.png)

[Screenshot 4](https://raw.githubusercontent.com/s-zeng/vim-settings/master/Pictures/Desktop3alt.png)

[Screenshot 5](https://raw.githubusercontent.com/s-zeng/vim-settings/master/Pictures/screenfetch.png)

[Wallpaper](https://raw.githubusercontent.com/s-zeng/vim-settings/master/Pictures/Wallpaper.jpg)
